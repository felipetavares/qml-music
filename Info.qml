import QtQuick 2.5

Item {
    property string song: '--'
    property string album: '--'
    property string artist: '--'

    // Song title
    SansText {
        id: songText

        font.pixelSize: 14
        font.bold: true
        color: 'white'

        text: parent.song
    }

    // Album title
    SansText {
        id: albumText

        anchors.top: songText.bottom

        font.pixelSize: 12
        font.bold: true
        color: '#ccf'

        text: parent.album
    }

    // Album 'by' Artist. This is the 'by'
    SansText {
        id: by

        anchors.top: songText.bottom; anchors.left: albumText.right
        anchors.leftMargin: 5

        font.pixelSize: 12
        font.bold: true
        color: 'white'

        text: 'by'
    }

    // Artist name
    SansText {
        id: artistText

        anchors.top: songText.bottom; anchors.left: by.right
        anchors.leftMargin: 5

        font.pixelSize: 12
        font.bold: true
        color: '#fcc'

        text: parent.artist
    }
}
