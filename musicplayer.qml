import QtQuick 2.5
import QtQuick.Window 2.0
import QtMultimedia 5.5
import Qt.labs.folderlistmodel 2.1

/*
    Root QML element, the window

    We use a window as root to be
    able to remove the borders
    (by using a frameless hint)
*/
Window {
    id: playerWindow

    // Size of the border+shadow in the background image
    // (gfx/bar.png)
    property var border_shadow_size: 11

    // Window size, same as gfx/bar.png
    width: 400
    height: 93

    // Disable window frames if the Window Manager
    // supports it
    flags: Window.FramelessWindowHint

    // Transparent default background allows us to
    // display the shadow around the window
    color: 'transparent'

    // Loads the OpenSans font from the fonts/ directory
    FontLoader {
        id: openSansLoader
        source: 'fonts/OpenSans-Semibold.ttf'
    }
    FontLoader {
        source: 'fonts/OpenSans-Regular.ttf'
    }

    // Decorate the window with the background image
    BorderImage {
        source: 'gfx/bar.png'

        // Borders include shadow
        border.left: border_shadow_size; border.top: border_shadow_size
        border.right: border_shadow_size; border.bottom: border_shadow_size

        // Fill the whole window
        anchors.fill: parent
    }

    /*
        This item just translates the origin
        a bit so it is inside the visible
        part of the window, not on the shadow
        or on the border.
    */
    Item {
        anchors.fill: parent; anchors.margins: border_shadow_size

        // Album cover
        Cover {
            id: cover

            // Left/Top side with a little border around
            anchors.left: parent.left; anchors.top: parent.top
            anchors.margins: 5
        }

        // Player controls
        Controls {
            id: controls

            // Glue to the cover right side/window's top
            anchors.left: cover.right; anchors.top: parent.top
            anchors.margins: 10; anchors.topMargin: 15
        }

        // Album and Song information
        Info {
            id: info

            // Put between the controls and the right end of the window
            anchors.top: parent.top; anchors.bottom: slider.top
            anchors.left: controls.right; anchors.right: parent.right
            anchors.leftMargin: 15; anchors.topMargin: 5
        }

        // Time from beginning
        SansText {
            id: fromBeginning

            text: '00:00'
            color: 'white'

            font.pixelSize: 12

            anchors.left: cover.right; anchors.bottom: parent.bottom
            anchors.margins: 5
        }

        // Time until end
        SansText {
            id: untilEnd

            text: '00:00'
            color: 'white'

            font.pixelSize: 12

            anchors.right: parent.right; anchors.bottom: parent.bottom
            anchors.margins: 5
        }

        // Slider
        Slider {
            id: slider

            // Put between the two time stamps
            anchors.left: fromBeginning.right; anchors.right: untilEnd.left
            // At the bottom
            anchors.bottom: parent.bottom
            anchors.margins: 7
        }
    }

    /*
        Logical/functional part of the application
    */
    Audio {
        id: mediaPlayer
        autoLoad: true

        onStatusChanged: {
            // Makes sure the metadata is available
            if (status === Audio.Buffered) {
                // access it
                var title = metaData.title;
                var album = metaData.albumTitle;
                var artist = metaData.albumArtist ||
                             metaData.contributingArtist;

                // set the information
                info.song = title;
                info.album = album;
                info.artist = artist;

                // set the cover
                var coverPath = 'music/'+artist.toLowerCase()+'_'+album.toLowerCase()+'.png';
                cover.source = coverPath.replace(/ /g, '_');
            }
        }

        onPlaybackStateChanged: {
            // Syncs the play/pause button with the song state
            controls.playing = playbackState === Audio.PlayingState;
        }

        onPositionChanged: {
            // Assign to the slider position the normalized
            // position on the song
            slider.position = position/duration;

            fromBeginning.text = timeFromBeginning();
            untilEnd.text = timeUntilEnd();
        }

        // Convert milisseconds to minutes:seconds
        function miliToMinSec (mili) {
            // Seconds
            var sec = Math.floor((mili/1000)%60);
            // Minutes
            var min = Math.floor(mili/60000);

            // Too many minutes. Conver to hours/minutes.
            if (min > 99) {
                var hours = Math.floor(min/60);
                min = min%60;

                if (min <= 9)
                    min = '0'+min;
                if (hours <= 9)
                    hours = '0'+hours;

                return hours+'h'+min;
            }

            if (sec <= 9)
                sec = '0'+sec;
            if (min <= 9)
                min = '0'+min;

            return min+':'+sec;
        }

        function timeFromBeginning () {
            return miliToMinSec(position);
        }

        function timeUntilEnd () {
            var value = duration-position;

            // Makes sure rounding errors don't
            // end up making negative times until end.
            if (value < 0)
                value = 0;

            return miliToMinSec(value);
        }
    }

    // Creates a list of all mp3 files in the music
    // folder
    FolderListModel {
        id: songList

        folder: 'music'
        nameFilters: [
            '*.mp3'
        ]

        // When at least one song name is loaded
        onCountChanged: {
            // Just loads the first song into the audio
            // element.
            playlist.loadItem();
        }
    }

    // Creates a playlist from the list above
    Playlist {
        id: playlist

        mediaPlayer: mediaPlayer
        list: songList
    }
}

