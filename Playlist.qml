import QtQuick 2.0
import QtMultimedia 5.5

/*
    The playlist. A simple list linked to the media player with
    previous and next functions.
*/
Item {
    property Audio mediaPlayer
    property var list

    // Current index in the list
    property int index: 0

    function loadItem (keepGoing) {
        // Get the path from the list
        var path = list.get(index, 'filePath');
        // Set the source on the mediaPlayer
        mediaPlayer.source = path;

        // Play it
        mediaPlayer.play();
        // if we shan't keep going, also pause (we do this play/pause
        // thing to make sure the metadata is loaded)
        if (!keepGoing) {
            mediaPlayer.pause();
        }
    }

    // Next song in the list
    function next (keepGoing) {
        // Makes sure we are not crossing boundaries
        if (index+1 < list.count) {
            // Next
            index++;

            loadItem(keepGoing);

            // Animate the button if we were called from the interface
            return true;
        }

        return false;
    }

    function previous (keepGoing) {
        if (index-1 >= 0) {
            index--;

            loadItem(keepGoing);

            return true;
        }

        return false;
    }
}
