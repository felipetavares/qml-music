import QtQuick 2.5

/*
    The usual text component, but with a custom font
*/
Text {
    font.family: openSansLoader.name
}
