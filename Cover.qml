import QtQuick 2.5

/*
    This component just displays a image (the albuns' cover)
    with a frame and an overlay.
*/
Item {
    // Url of the image that should be used as cover
    property string source
    // Same as the frame image
    width: 62; height: 62

    // Frame
    Image {
        source: 'gfx/cover_frame.png'

        anchors.fill: parent
    }

    // Cover image
    Image {
        id: coverImage

        // Margins to allow the frame to appear a bit
        anchors.fill: parent; anchors.margins: 2

        source: parent.source
    }

    // Overlay (gives a bit of reflection and a nice border)
    Image {
        source: 'gfx/cover_overlay.png'

        // Same as coverImage to make them fit perfectly
        anchors.fill: parent; anchors.margins: 2
    }
}
