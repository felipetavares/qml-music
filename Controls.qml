import QtQuick 2.5
import QtMultimedia 5.5

/*
    Provides play/pause/next/previous building over
    'Icon' components.
*/
Item {
    property bool playing: false
    width: 64

    // 'Previous' icon
    Icon {
        id: previous
        source: 'icons/rewind.png'

        // Same as the prev image (icons/rewind.png)
        width: 13; height: 15
        // Because prev/next icons are smaller than play/pause
        // we need to align them vertically with a little offset
        y: 5
    }

    Icon {
        id: playPause
        source: 'icons/play.png'

        anchors.left: previous.right; anchors.leftMargin: 10
        // Same as the play/pause images
        width: 18; height: 24
    }

    Icon {
        id: next
        source: 'icons/forward.png'

        anchors.left: playPause.right; anchors.leftMargin: 10
        width: 13; height: 15
        y: 5
    }

    // Add callbacks
    Component.onCompleted: {
        // These callbacks return true if there should
        // be an animation, false if not (to inform the
        // user if an action was executed)
        playPause.callback = function () {
            var playing = (mediaPlayer.playbackState === Audio.PlayingState);

            if (playing) {
                mediaPlayer.pause();
            } else {
                mediaPlayer.play();
            }

            return true;
        }

        previous.callback = function () {
            // Previous and keep playing if we were already playing
            return playlist.previous(mediaPlayer.playbackState === Audio.PlayingState);
        }

        next.callback = function () {
            // Next and keep playing if we were already playing
            return playlist.next(mediaPlayer.playbackState === Audio.PlayingState);
        }
    }

    /*
        Allows for the playPause icon to be changed from
        the outside.

        This allows for things like automatically putting
        a pause icon when the song finishes playing.
    */
    onPlayingChanged: {
        if (playing) {
            playPause.source = 'icons/pause.png';
        } else {
            playPause.source = 'icons/play.png';
        }
    }
}
