import QtQuick 2.5

Item {
    property var position: 0

    // Same as the image (gfx/slider_background.png)
    height: 11

    // Background
    BorderImage {
        source: 'gfx/slider_background.png'

        border.left: 8; border.top: 2
        border.right: 8; border.bottom: 2

        anchors.fill: parent
    }

    // Played (blue) part
    BorderImage {
        id: playedPart

        source: 'gfx/slider_value_left.png'

        border.left: 8; border.top: 2
        border.right: 8; border.bottom: 2

        anchors.top: parent.top; anchors.topMargin: 1
        // Same as the image
        height: 9
        // Starts at position 0
        width: 0
    }

    // Knob
    Image {
        id: knob

        source: 'gfx/slider_knob.png'
        // Move a bit up so it is centered
        x: 0; y: -3
    }

    // Handle clicks
    MouseArea {
        anchors.fill: parent

        // When the mouse moves horizontally
        onMouseXChanged: {
            // check if it is inside and if a button is pressed
            if (containsMouse && pressed) {
                var normalizedPosition;

                // Calculate the position between 0-1 of the click based on the full width.
                // As the knob cannot move to absolute zero/the full width
                // because it is circular and would be outside boundaries in both these cases,
                // we remove from the full width
                // the size of the knob and balance the mouse position by
                // half of the knob size
                normalizedPosition = (mouseX-knob.width/2)/
                                     (parent.width-knob.width);

                // Avoid annoying noises while seeking
                mediaPlayer.volume = 0;
                // Seek
                mediaPlayer.seek(mediaPlayer.duration*normalizedPosition);
            }
        }

        // When the mouse is released
        onClicked: {
            // Makes sure the volume is back to the usual
            mediaPlayer.volume = 1;
        }

        onContainsMouseChanged: {
            // Makes sure if the mouse leaves the slider
            // the colume is back to the usual.
            if (!containsMouse) {
                mediaPlayer.volume = 1;
            }
        }
    }

    // Adjust the knob/played part positions
    // when the position property is changed
    onPositionChanged: {
        // We subtract the knob size here to make sure it is within our boundaries
        // add 1/4 knob width to make sure the played part right end is below the knob
        playedPart.width = position*(width-knob.width)+knob.width/4;
        knob.x = position*(width-knob.width);
    }
}
