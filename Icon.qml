import QtQuick 2.5

Item {
    property var callback: null
    property string source

    // The icon's image
    Image {
        id: image

        source: parent.source
        anchors.fill: parent

        // The 'clicked' animation. Makes the image smaller
        // and the the normal size again
        SequentialAnimation on anchors.margins {
            id: clickAnimation
            // Animations usually start running
            running: false

            // Grows the margin
            NumberAnimation {
                from: 0; to: 5
                duration: 150

                // InOut feels good. Quad because it's
                // light.
                easing.type: Easing.InOutQuad
            }

            // Shrinks the margin
            NumberAnimation {
                from: 5; to: 0
                duration: 150

                easing.type: Easing.InOutQuad
            }
        }
    }

    MouseArea {
        anchors.fill: parent

        onClicked: {
            if (parent.callback) {
                // If the callback returns true
                if (parent.callback()) {
                    // we animate to let the user know something
                    // happened
                    clickAnimation.running = true;
                }
            }
        }
    }
}
